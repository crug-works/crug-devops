
<# 
   PowerShell script to run command line calls with echoed timestamps
   and redirects console output and error to external log file.
#>

cd "C:\path\to\scripts"

& {
    echo "`nAutomation Start: $(Get-Date -format 'u')"

    # RUN R SCRIPT
    echo "`n****************************************************"
    echo "`nSTEP 1: myScript.R - $(Get-Date -format 'u')"
    echo "`n****************************************************"

    Rscript "myScript.R"


    # RENDER R MARKDOWN
    echo "`n****************************************************"
    echo "`nSTEP 2: myMarkdown.Rmd - $(Get-Date -format 'u')"
    echo "`n****************************************************"

    Rscript -e "library(rmarkdown); render(`"myMarkdown.Rmd`")"
    Invoke-Item "myMarkdown.html"


    # LAUNCH R SHINY
    echo "`n****************************************************"
    echo "`nSTEP 3: myShinyApp.R - $(Get-Date -format 'u')"
    echo "`n****************************************************"

    Rscript -e "library(shiny); runApp(`"myShinyApp.R`")"

    echo "`nAutomation End: $(Get-Date -format 'u')"

} 3>&1 2>&1 > "C:\path\to\logs\automation_run_$(Get-Date -format 'yyyyMMdd').log"
