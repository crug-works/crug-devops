# CRUG DevOps

![CRUG logo](crug_skyline.png)

## Repository for CRUG scripts and projects involving devops including:

- ### Automation
- ### CI/CD
- ### Migration
- ### Packaging
- ### Pipelines
- ### Workflows

