#######################################
### CTA Bus and Rail Ridership API 
###   Multi-Core Parallel Process
#######################################

### GLOBALS
library(iterators)
library(parallel)
library(foreach)
library(doParallel)

CHI_APP_TOKEN <- Sys.getenv("CHI_APP_TOKEN")

cta_rail_stations <- read.csv(
  paste0(
    "https://data.cityofchicago.org/resource/8pix-ypme.csv?",
    "$limit=2000&$offset=0&$$app_token=", CHI_APP_TOKEN
  )
)

### PARALLEL METHODS
retrieve_cta_bus_ridership <- function(i) {
  read.csv(
    paste0(
      "https://data.cityofchicago.org/resource/jyb9-n7fm.csv?",
      "$limit=2000&$offset=",
      as.integer(i * 2000), 
      "&$$app_token=", 
      CHI_APP_TOKEN
    )
  )
}

retrieve_cta_rail_ridership <- function(i) {
  read.csv(
    paste0(
      "https://data.cityofchicago.org/resource/5neh-572f.csv?",
      "$limit=2000&$offset=",
      as.integer(i * 2000), 
      "&$$app_token=", 
      CHI_APP_TOKEN
    )
  )
}

### PARALLEL SETUP
if(file.exists("cta_ridership_parallelization.log"))
  unlink("cta_ridership_parallelization.log")

num_cores <- 4
registerDoParallel(num_cores)
cl = makeCluster(
  num_cores, outfile="cta_ridership_parallelization.log"
) 

### PARALLEL RUN
num_loops <- (1E6 + 2E5) / 2E3
cta_rail_ridership <- foreach(
  i = 0:(num_loops-1), .combine = rbind.data.frame
) %dopar% {
  retrieve_cta_rail_ridership(i)
}

num_loops <- 1E6 / 2E3
cta_bus_ridership <- foreach(
  i = 0:(num_loops-1), .combine = rbind.data.frame
) %dopar% {
  retrieve_cta_bus_ridership(i)
}

stopCluster(cl)
rm(cl)
invisible(gc())


### SAVE OUTPUT

str(cta_rail_stations)
# 'data.frame':	300 obs. of  17 variables:
# $ stop_id                 : int  30162 30161 30022 30023 30214 30213 30245 30246 30237 30209 ...
# $ direction_id            : chr  "W" "E" "N" "S" ...
# $ stop_name               : chr  "18th (54th/Cermak-bound)" "18th (Loop-bound)" "35th/Archer (Loop-bound)" "35th/Archer (Midway-bound)" ...
# $ station_name            : chr  "18th" "18th" "35th/Archer" "35th/Archer" ...
# $ station_descriptive_name: chr  "18th (Pink Line)" "18th (Pink Line)" "35th/Archer (Orange Line)" "35th/Archer (Orange Line)" ...
# $ map_id                  : int  40830 40830 40120 40120 41120 41120 41270 41270 41230 41080 ...
# $ ada                     : chr  "true" "true" "true" "true" ...
# $ red                     : chr  "false" "false" "false" "false" ...
# $ blue                    : chr  "false" "false" "false" "false" ...
# $ g                       : chr  "false" "false" "false" "false" ...
# $ brn                     : chr  "false" "false" "false" "false" ...
# $ p                       : chr  "false" "false" "false" "false" ...
# $ pexp                    : chr  "false" "false" "false" "false" ...
# $ y                       : chr  "false" "false" "false" "false" ...
# $ pnk                     : chr  "true" "true" "false" "false" ...
# $ o                       : chr  "false" "false" "true" "true" ...
# $ location                : chr  "\n,  \n(41.857908, -87.669147)" "\n,  \n(41.857908, -87.669147)" "\n,  \n(41.829353, -87.680622)" "\n,  \n(41.829353, -87.680622)" ...

saveRDS(cta_rail_stations, "cta_rail_stations.rds")

str(cta_rail_ridership)
# 'data.frame':	1171469 obs. of  5 variables:
# $ station_id : int  40350 41130 40760 40070 40090 40590 40720 41260 40230 41120 ...
# $ stationname: chr  "UIC-Halsted" "Halsted-Orange" "Granville" "Jackson/Dearborn" ...
# $ date       : chr  "2001-01-01T00:00:00.000" "2001-01-01T00:00:00.000" "2001-01-01T00:00:00.000" "2001-01-01T00:00:00.000" ...
# $ daytype    : chr  "U" "U" "U" "U" ...
# $ rides      : int  273 306 1059 649 411 870 391 399 788 448 ...

saveRDS(cta_rail_ridership, "cta_rail_ridership.rds")


str(cta_bus_ridership)
# 'data.frame':	991181 obs. of  4 variables:
#  $ route  : chr  "1" "1" "1" "1" ...
#  $ date   : chr  "2001-01-02T00:00:00.000" "2001-01-03T00:00:00.000" "2001-01-04T00:00:00.000" "2001-01-05T00:00:00.000" ...
#  $ daytype: chr  "W" "W" "W" "W" ...
#  $ rides  : int  5813 6809 6907 6154 6126 7823 6398 6212 8178 3561 ...

saveRDS(cta_bus_ridership, "cta_bus_ridership.rds")
