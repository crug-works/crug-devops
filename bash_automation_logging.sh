#!/usr/bin/env bash

: '
   Bash script to run command line calls with echoed timestamps
   and redirects console output and error to external log file.
'

cd "/path/to/scripts"

{
    echo -e "\nAutomation Start: $(date '+%Y-%m-%d %H:%M:%S')"

    # RUN R SCRIPT
    echo -e "\n*******************************************************"
    echo -e "\nSTEP 1: myScript.R - $(date '+%Y-%m-%d %H:%M:%S')"
    echo -e "\n*******************************************************"

    Rscript "myScript.R"


    # RENDER R MARKDOWN
    echo -e "\n*******************************************************"
    echo -e "\nSTEP 2: myMarkdown.Rmd - $(date '+%Y-%m-%d %H:%M:%S')"
    echo -e "\n*******************************************************"

    Rscript -e "library(rmarkdown); render(\"myMarkdown.Rmd\")"
    Invoke-Item "myMarkdown.html"


    # LAUNCH R SHINY
    echo -e "\n*******************************************************"
    echo -e "\nSTEP 3: myShinyApp.R - $(date '+%Y-%m-%d %H:%M:%S')"
    echo -e "\n*******************************************************"

    Rscript -e "library(shiny); runApp(\"myShinyApp.R\")"

    echo -e "\n\nAutomation End: $(date '+%Y-%m-%d %H:%M:%S')"

} &> "/path/to/automation_$(date '+%Y_%m_%d').log"
